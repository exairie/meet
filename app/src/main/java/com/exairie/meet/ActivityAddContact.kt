package com.exairie.meet

import android.app.ProgressDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add_contact.*
import kotlinx.android.synthetic.main.view_search_user.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivityAddContact : ExtActivity(R.layout.activity_add_contact) {
    val adapter = Adapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        hasBackButton(true)
        setupSearch()
        recycler_people_search.adapter = adapter
        recycler_people_search.layoutManager = LinearLayoutManager(this)
    }

    private fun setupSearch() {
        val startSearch = Runnable{
            search()
        }
        val handler = Handler(Looper.getMainLooper())
        t_email.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) = Unit

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int){
                handler.removeCallbacks(startSearch)
                handler.postDelayed(startSearch,1000)
            }
        })
    }

    private fun search() {
        val searchData = HashMap<String,String>()
        searchData.put("s",t_email.text.toString())
        ApiClient.getApi().searchUsers(searchData).enqueue(object : Callback<Array<ApiData.LoginData>>{
            override fun onFailure(call: Call<Array<ApiData.LoginData>>?, t: Throwable?) {
                Toast.makeText(this@ActivityAddContact,"Unable to search for users!",Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Array<ApiData.LoginData>>?, response: Response<Array<ApiData.LoginData>>?) {
                if(response?.code() == 200){
                    adapter.data = response.body() ?: arrayOf()
                    recycler_people_search.post({
                        adapter.notifyDataSetChanged()
                    })
                }
            }
        })
    }
    private fun addUser(id: Long) {
        setResult(1)
        val dialog = ProgressDialog(this)
        dialog.setMessage("Adding user to your contact")
        dialog.setCancelable(false)
        runOnUiThread { dialog.show() }
        val user = getUserData()
        ApiClient.getApi().addContact(user?.id?:-1,id).enqueue(object : Callback<Any>{
            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                runOnUiThread {
                    AlertDialog.Builder(this@ActivityAddContact)
                            .setTitle("Login Error")
                            .setMessage("Unable to connect to server")
                            .setPositiveButton("Retry",{o,_->
                                o.dismiss()
                                addUser(id)
                            })
                            .show()
                }
                runOnUiThread { dialog.dismiss() }
            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                runOnUiThread { dialog.dismiss() }
                if(response?.code() == 201){
                    Toast.makeText(this@ActivityAddContact,"User added to your contacts!",Toast.LENGTH_LONG).show()
                    setResult(1)
                }
                else if(response?.code() == 202){
                    Toast.makeText(this@ActivityAddContact,"User already in your contacts!",Toast.LENGTH_LONG).show()
                }
                else{
                    Toast.makeText(this@ActivityAddContact,"User data invalid!",Toast.LENGTH_LONG).show()
                }
                Log.d("Error",""+response?.errorBody()?.string())
            }
        })
    }
    inner class Adapter : RecyclerView.Adapter<Holder>(){
        var data = arrayOf<ApiData.LoginData>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_search_user,parent,false)
            return Holder(v)
        }

        override fun getItemCount(): Int = data.size

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val d = data[position]
            try{
                Picasso.get().load(d.photo_url).placeholder(R.drawable.ic_account_circle_grey_300_24dp)
                        .error(R.drawable.ic_account_circle_grey_500_24dp)
                        .into(holder.itemView.photo)
            }catch (e : Exception){
                e.printStackTrace()
            }

            holder.itemView.l_email.setText(d.email)
            holder.itemView.l_initiator.setText(d.firstname)
            holder.itemView.btn_add_user.setOnClickListener {
                addUser(d.id)
            }

        }
    }
}
