package com.exairie.meet

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.exairie.meet.api.ApiClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_create_geofence.*

class ActivityCreateGeofence : ExtActivity(R.layout.activity_create_geofence), OnMapReadyCallback {

    data class LatLngPos(val latitude: Double, val longitude: Double)

    var centerLatLng: LatLng? = null
    val geofences = mutableListOf<LatLng>()
    var polygonTargetMarker = mutableListOf<Marker>()
    var currentMapPolygon: Polygon? = null
    var centerSelectionMarker: Marker? = null

    var readOnlyMode = false

    lateinit var centerMarker: Marker
    var map: GoogleMap? = null

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        this.map = p0
        map?.let { map ->

            val locationService = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val lastLocation = locationService.getLastKnownLocation(LocationManager.GPS_PROVIDER)

            map.isMyLocationEnabled = true
            centerMarker = map.addMarker(MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_my_location_blue_500_24dp))
                    .position(map.cameraPosition.target)
            )
            map.setOnCameraMoveListener {
                centerMarker.position = map.cameraPosition.target
            }

            lastLocation?.let {
                val update = CameraUpdateFactory.newLatLngZoom(LatLng(it.latitude, it.longitude), 14.0f)
                map.animateCamera(update)
            }

            if (readOnlyMode) setupInitialData()
        }
        setupButtons()
    }

    fun setupInitialData() {
        val centerJson = intent.getStringExtra("center")
        val geofencesJson = intent.getStringExtra("geofences")

        val center = ApiClient.getGson().fromJson(centerJson, LatLngPos::class.java)
        val geofences = ApiClient.getGson().fromJson(geofencesJson, Array<Array<Double>>::class.java)

        map?.let {
            centerSelectionMarker?.remove()
            centerSelectionMarker = it.addMarker(MarkerOptions().position(LatLng(center.latitude, center.longitude)).title("Center Position"))

            val bounds = LatLngBounds.Builder()

            val polygonOptions = PolygonOptions()
                    .fillColor(resources.getColor(R.color.polygonInside))
                    .strokeColor(resources.getColor(R.color.polygonBorder))
            geofences.forEach { pos ->
                val l = LatLng(pos[0], pos[1])
                polygonOptions.add(l)
                bounds.include(l)
            }

            it.addPolygon(polygonOptions)

            it.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 8))
        }

        title = intent.getStringExtra("destination_name")
        layout_buttons.visibility = View.GONE

    }

    fun setupButtons() {
        btn_set_center.setOnClickListener {
            map?.let {
                val center = it.cameraPosition.target
                centerSelectionMarker?.remove()
                centerSelectionMarker = it.addMarker(MarkerOptions().position(center).title("Center Position"))


            }
        }
        btn_add_geofence.setOnClickListener {
            addGeofence()
        }
        btn_clear_geofence.setOnClickListener {
            clearGeofence()
        }
    }

    fun addGeofence() {
        map?.let { map ->
            val center = map.cameraPosition.target
            geofences.add(center)
            currentMapPolygon?.remove()

            polygonTargetMarker.forEach {
                it.remove()
            }

            polygonTargetMarker.clear()

            val polygonOptions = PolygonOptions()
                    .fillColor(resources.getColor(R.color.polygonInside))
                    .strokeColor(resources.getColor(R.color.polygonBorder))
            geofences.forEach {
                polygonOptions.add(it)
                polygonTargetMarker.add(map.addMarker(MarkerOptions()
                        .position(it)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_arrow_drop_down_circle_blue_500_18dp))))
            }

            currentMapPolygon = map.addPolygon(polygonOptions)

        }
    }

    fun clearGeofence() {
        if (geofences.size < 1) return
        geofences.removeAt(geofences.size - 1)
        currentMapPolygon?.remove()

        val polygonOptions = PolygonOptions()
                .fillColor(resources.getColor(R.color.polygonInside))
                .strokeColor(resources.getColor(R.color.polygonBorder))

        polygonTargetMarker.forEach {
            it.remove()
        }

        polygonTargetMarker.clear()

        geofences.forEach {
            polygonOptions.add(it)
            map?.let { map ->
                polygonTargetMarker.add(map.addMarker(MarkerOptions()
                        .position(it)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_arrow_drop_down_circle_blue_500_18dp))))
            }
        }
        if (geofences.size > 1)
            currentMapPolygon = map?.addPolygon(polygonOptions)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        title = "Create geofence"

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map_geofence_activity) as SupportMapFragment

        mapFragment.getMapAsync(this)

        readOnlyMode = intent.getBooleanExtra("readonly", false)


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (!readOnlyMode) menuInflater.inflate(R.menu.menu_create_meeting, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mn_create -> {
                saveResult()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveResult() {
        val i = Intent()
        if (centerSelectionMarker == null) {
            AlertDialog.Builder(this)
                    .setTitle("Invalid Position")
                    .setMessage("Please check your center position!")
                    .setNegativeButton("Close") { d, _ -> d.dismiss() }
                    .show()
            return
        }

        centerSelectionMarker?.let {
            val pos = LatLngPos(it.position.latitude, it.position.longitude)
            i.putExtra("center", ApiClient.getGson().toJson(pos))
        }

        if (geofences.size < 3) {
            AlertDialog.Builder(this)
                    .setTitle("Invalid Geofence")
                    .setMessage("Geofence coordinate must consist of minimum 3 points!")
                    .setNegativeButton("Close") { d, _ -> d.dismiss() }
                    .show()
            return
        }

        val geoPositions = mutableListOf<LatLngPos>()
        geofences.forEach {
            geoPositions.add(LatLngPos(it.latitude, it.longitude))
        }

        i.putExtra("geofences", ApiClient.getGson().toJson(geoPositions))

        setResult(100, i)
        finish()
    }

}
