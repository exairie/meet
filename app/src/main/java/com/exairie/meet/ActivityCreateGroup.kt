package com.exairie.meet

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import kotlinx.android.synthetic.main.activity_create_group.*

class ActivityCreateGroup : ExtActivity(R.layout.activity_create_group) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)

        title = "Create Tour Group"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_group, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mn_continue_create_group -> {
                continueCreateGroup()
            }
        }
        return true
    }

    private fun continueCreateGroup() {
        if (text_group_name.text.length < 5) {
            Toast.makeText(this, "Group name must be 5 character or more", Toast.LENGTH_LONG).show()
            return
        }
        if (text_group_description.text.length < 10) {
            Toast.makeText(this, "Group description must be 10 character or more", Toast.LENGTH_LONG).show()
            return
        }
        val groupdata = ApiData.GroupInput(
                active = 1,
                group_name = text_group_name.text.toString(),
                description = text_group_description.text.toString(),
                initiator = getUserData()?.id ?: -1,
                members = mutableListOf()
        )

        val i = Intent(this, ActivityCreateMeetingMulti::class.java)
        i.putExtra("groupdata", ApiClient.getGson().toJson(groupdata))

        startActivity(i)
    }
}
