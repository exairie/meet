package com.exairie.meet

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.*
import android.widget.RelativeLayout
import android.widget.Toast
import com.exairie.meet.api.ApiAccessInterface
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import com.exairie.meet.api.GoogleMapsApi
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_create_meeting.*
import java.io.IOException
import java.util.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.sleepbot.datetimepicker.time.TimePickerDialog
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.HashMap


class ActivityCreateMeeting : ExtActivity(R.layout.activity_create_meeting), OnMapReadyCallback{
    lateinit var mapFragment : SupportMapFragment
    lateinit var map : GoogleMap
    lateinit var marker : Marker
    lateinit var bottomSheetSearch : BottomSheetBehavior<View>
    val position = Location("")

    var eta : Calendar = Calendar.getInstance()

    lateinit var meetingMembers : Array<ApiData.LoginData>

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        this.map = p0!!
        map.setPadding(8,8,8,layout_address.height + 10)
        map.isMyLocationEnabled = true
        decodePosition()
        position.longitude = map.cameraPosition.target.longitude
        position.latitude = map.cameraPosition.target.latitude
        marker = map.addMarker(MarkerOptions()
                .flat(true)
                .position(map.cameraPosition.target)
                .title("Select this point"))
        map.uiSettings.isMyLocationButtonEnabled = true
        map.setOnCameraMoveListener(object : GoogleMap.OnCameraMoveListener{
            override fun onCameraMove() {
                marker.position = map.cameraPosition.target
            }
        })
        map.setOnCameraIdleListener {
//            Toast.makeText(this,"Camera is finished moving",Toast.LENGTH_LONG).show()
            decodePosition()
        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_meeting,menu)
        return true
    }
    fun decodePosition(){
        val pos = map.cameraPosition.target

        geoCode(pos.latitude,pos.longitude)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        setTitle("Create a meeting")
        hasBackButton(true)

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)
        bottomSheetSearch  = BottomSheetBehavior.from(bottomsheet)


        layout_address.setOnClickListener {
            bottomSheetSearch.state = BottomSheetBehavior.STATE_EXPANDED
        }
        btn_startgeocode.setOnClickListener({
            getLocationFromAddress(t_search_location.text.toString())
        })

        meetingMembers = ApiClient.getGson().fromJson(intent.getStringExtra("users"),Array<ApiData.LoginData>::class.java)

        updateUI()
    }

    private fun updateUI() {
        layout_profiles.removeAllViews()

        val user = getUserData()
        user!!.firstname.let{
            val splitted = user!!.firstname!!.split(" ")
            l_initiator.text = if(splitted.size > 0) splitted[0] else "-"
        }
        try{
            Picasso.get().load(user.photo_url)
                    .placeholder(R.drawable.ic_account_circle_grey_300_24dp)
                    .error(R.drawable.ic_account_circle_grey_500_24dp)
                    .into(img_profile_initiator)
        }catch (e : Exception){
            e.printStackTrace()
        }

        if(meetingMembers.size == 1){
            meetingMembers[0].firstname.let{
                val splitted = meetingMembers[0].firstname!!.split(" ")
                l_members.text = if(splitted.size > 0) splitted[0] else "-"
            }
        }else{
            l_members.text = "${meetingMembers.size} People(s)"
        }

        for(i in 0 until meetingMembers.size){
            val c = CircleImageView(this)
            val wInt = (50 * resources.displayMetrics.density).toInt()
            val lp = RelativeLayout.LayoutParams(wInt,wInt)
            lp.setMargins(i * 15,0,0,0)
            c.layoutParams = lp

            try{
                Picasso.get().load(meetingMembers[i].photo_url)
                        .placeholder(R.drawable.ic_account_circle_grey_300_24dp)
                        .error(R.drawable.ic_account_circle_grey_500_24dp)
                        .into(c)
            }catch (e : Exception){
                e.printStackTrace()
            }

            layout_profiles.addView(c)
        }

        val now = Calendar.getInstance()

        t_eta.setOnFocusChangeListener({v,f ->
            if(f){
                val picker = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(object : com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener{
                    override fun onDateSet(p0: com.fourmob.datetimepicker.date.DatePickerDialog?, p1: Int, p2: Int, p3: Int) {
                        val selected = Calendar.getInstance()
                        selected.set(p1,p2,p3,0,0,0)
                        eta = selected
                        t_eta.setText(android.text.format.DateFormat.format("dd-MM-yyyy hh:mm::ss",eta))

                        TimePickerDialog.newInstance({l,h,m ->
                            eta.set(Calendar.MINUTE,m)
                            eta.set(Calendar.HOUR,h)
                            eta.set(Calendar.SECOND,0)
                        },now.get(Calendar.HOUR),now.get(Calendar.MINUTE),true).show(supportFragmentManager,"TIMEPICKER")

                        t_eta.setText(android.text.format.DateFormat.format("dd-MM-yyyy hh:mm::ss",eta))
                    }
                },now.get(Calendar.YEAR),now.get(Calendar.MONTH),now.get(Calendar.DAY_OF_MONTH))
                picker.show(supportFragmentManager,"DATEPICKER")
            }
        })
    }

    fun geoCode(lat : Double, lng : Double){
        val context : Context? = this
        val task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Void, Void, List<Address>?>(){
            override fun doInBackground(vararg params: Void?): List<Address>? {
                var addresses : List<Address>? = null
                val geocoder = Geocoder(context, Locale.ENGLISH)
                try {
                    addresses = geocoder.getFromLocation(lat,lng,3)

                    if(addresses.size > 0){
                        val address = addresses[0]
                        var strAddress = mutableListOf<String>()
                        if(address.adminArea ?: "" != ""){
                            strAddress.add(address.adminArea)
                        }
                        if(address.thoroughfare ?: "" != ""){
                            strAddress.add(address.thoroughfare)
                        }
                        if(address.countryName ?: "" != ""){
                            strAddress.add(address.countryName)
                        }

                        if(context!= null){
                            runOnUiThread {
                                location_text.setText(strAddress.joinToString())
                            }
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                return null
            }

        }
        task.execute()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.mn_create -> {
                createMeeting()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createMeeting() {
        val dialog = ProgressDialog(this)
        dialog.setMessage("Creating Meeting...")
        dialog.setCancelable(false)
        runOnUiThread { dialog.show() }
        val user = getUserData()

        val members = mutableListOf<String>()
        members.add((getUserData()?.id ?: -1).toString())
        members.addAll(meetingMembers.map { m->m.id.toString() })
//        val meetingTarget = meetingMembers.map { m -> m.id.toString() }.toTypedArray()

        val meetingData = ApiData.Meeting(-1,user?.id ?: -1,eta.time,"active",Date(),Date(),map.cameraPosition.target.latitude,map.cameraPosition.target.longitude,null,null)

        val meetingRequest = ApiData.MeetingRequest(meetingData,members.toTypedArray())

        ApiClient.getApi().createMeeting(meetingRequest).enqueue(object : Callback<ApiData.Meeting>{
            override fun onFailure(call: Call<ApiData.Meeting>?, t: Throwable?) {
                runOnUiThread {
                    AlertDialog.Builder(this@ActivityCreateMeeting)
                            .setTitle("Error")
                            .setMessage("Unable to process request")
                            .setPositiveButton("Retry",{o,_->
                                o.dismiss()
                                createMeeting()
                            })
                            .show()
                }
                runOnUiThread { dialog.dismiss() }
            }

            override fun onResponse(call: Call<ApiData.Meeting>?, response: Response<ApiData.Meeting>?) {

                runOnUiThread { dialog.dismiss() }
                if(response?.code() == 201){
                    setInMeeting(response.body())

                    val i = Intent(this@ActivityCreateMeeting,ActivityInMeeting::class.java)
                    i.putExtra("meeting",ApiClient.getGson().toJson(response.body()))
                    startActivity(i)
                    finish()
                }else{
                    runOnUiThread {
                        AlertDialog.Builder(this@ActivityCreateMeeting)
                                .setTitle("Error")
                                .setMessage("Server error occured")
                                .setPositiveButton("Retry",{o,_->
                                    o.dismiss()
                                    createMeeting()
                                })
                                .show()
                    }

                    Log.d("Error",""+response?.errorBody()?.string() ?: "")
                }
            }
        })
    }

    fun getLocationFromAddress(strAddress: String) {
        //Create coder with Activity context - this
        val hmap = HashMap<String,String>()
        hmap.put("address",strAddress)
        hmap.put("sensor","false")
        ApiClient.getGoogleClient()
                .create(GoogleMapsApi::class.java)
                .geocodeAddress(hmap)
                .enqueue(object : Callback<ApiData.GoogleMapGeocodeResult> {
            override fun onFailure(call: Call<ApiData.GoogleMapGeocodeResult>?, t: Throwable?) {
                Toast.makeText(this@ActivityCreateMeeting,"Unable to find location :(",Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ApiData.GoogleMapGeocodeResult>?, response: Response<ApiData.GoogleMapGeocodeResult>?) {
                if(response?.code() == 200){
                    if(response?.body()?.status == "OK"){
                        if(response.body()?.results?.size ?: 0 < 1){
                            Toast.makeText(this@ActivityCreateMeeting,"Unable to find that location :(",Toast.LENGTH_LONG).show()
                        }else{
                            val result = response.body()!!.results[0]
                            result.geometry.location
                            val latLng = LatLng(result.geometry.location.lat, result.geometry.location.lng)

                            //Put marker on map on that LatLng
                            //val srchMarker = mMap.addMarker(MarkerOptions().position(latLng).title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.bb)))

                            //Animate and Zoon on that map location
                            map.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                            map.animateCamera(CameraUpdateFactory.zoomTo(15f))
                        }
                    }
                }else{
                    Toast.makeText(this@ActivityCreateMeeting,"Error retrieving location",Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}
