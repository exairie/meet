package com.exairie.meet

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.LinearLayout
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_create_meeting_multi.*
import kotlinx.android.synthetic.main.fragment_contactitem.view.*
import kotlinx.android.synthetic.main.view_contact_selected.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ActivityCreateMeetingMulti : ExtActivity(R.layout.activity_create_meeting_multi) {
    val adapter = Adapter()
    val selAdapter = SelectedContactAdapter()
    lateinit var groupData: ApiData.GroupInput
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        try {
            val groupDataJson = intent?.getStringExtra("groupdata")
            groupData = ApiClient.getGson().fromJson(groupDataJson, ApiData.GroupInput::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
            finish()
        }

        setTitle("Members for ${groupData.group_name}")

        hasBackButton(true)

        recycler_contactitem.layoutManager = LinearLayoutManager(this)
        recycler_contactitem.adapter = adapter

        recycler_selectedcontact.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        recycler_selectedcontact.adapter = selAdapter

        loadContacts()

        btn_start.setOnClickListener {
            createMeeting()
        }
    }

    fun loadContacts() {
        ApiClient.getApi().getContacts(getUserData()?.id
                ?: -1).enqueue(object : Callback<Array<ApiData.Contact>> {
            override fun onFailure(call: Call<Array<ApiData.Contact>>?, t: Throwable?) {
                launch(UI) {
                    AlertDialog.Builder(this@ActivityCreateMeetingMulti)
                            .setTitle("Loading Error")
                            .setMessage("Unable to get your contacts")
                            .setPositiveButton("Retry", { o, _ ->
                                o.dismiss()
                                loadContacts()
                            })
                            .show()
                }

            }

            override fun onResponse(call: Call<Array<ApiData.Contact>>?, response: Response<Array<ApiData.Contact>>?) {
                if (response?.code() == 200) {
                    adapter.data.clear()
                    adapter.data.addAll(response.body() ?: arrayOf())
                    recycler_contactitem.post({
                        adapter.notifyDataSetChanged()
                    })
                } else {
                    AlertDialog.Builder(this@ActivityCreateMeetingMulti)
                            .setTitle("Loading Error")
                            .setMessage("Unable to fetch your contacts")
                            .setPositiveButton("Retry") { o, _ ->
                                o.dismiss()
                                loadContacts()
                            }
                            .show()
                }
            }
        })
    }

    fun createMeeting() {
        val progress = ProgressDialog(this)
        progress.setTitle("Please Wait")
        progress.setMessage("Saving Group...")
        progress.show()
        if (selAdapter.data.size < 1) {
            AlertDialog.Builder(this)
                    .setTitle("Empty Members")
                    .setMessage("You should select at least one group member")
                    .setPositiveButton("Close", { d, _ -> d.dismiss() })
                    .show()
            return
        }
//        val i = Intent(this, ActivityCreateMeeting::class.java)
//        i.putExtra("users", ApiClient.getGson().toJson(selAdapter.data.map { c -> c.contact }.toTypedArray()))
//        startActivity(i)
//        finish()

        selAdapter.data.forEach {
            it.contact?.let { c->groupData.members.add(c.id) }
        }

        ApiClient.getApi().createGroupWithMember(getUserData()?.id ?: -1, groupData)
                .enqueue(object : Callback<ApiData.Group> {
                    override fun onFailure(call: Call<ApiData.Group>, t: Throwable) {
                        t.printStackTrace()
                        runOnUiThread {
                            progress.dismiss()
                        }
                    }

                    override fun onResponse(call: Call<ApiData.Group>, response: Response<ApiData.Group>) {
                        runOnUiThread {
                            progress.dismiss()
                        }
                        if (response.code() == 200) {
                            Toast.makeText(this@ActivityCreateMeetingMulti, "Group Created!", Toast.LENGTH_LONG).show()
                            this@ActivityCreateMeetingMulti.finish()
                        }else{
                            Log.d("ERR",response.errorBody()?.string() ?: "")
                        }
                    }
                })
    }

    inner class Adapter : RecyclerView.Adapter<Holder>() {
        val data = mutableListOf<ApiData.Contact>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.fragment_contactitem, parent, false)
            return Holder(v)
        }

        override fun getItemCount(): Int = data.size

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val cdata = data[position]
            with(holder.itemView) {
                try {
                    Picasso.get().load(cdata.contact?.photo_url
                            ?: "").placeholder(R.drawable.ic_account_circle_grey_300_24dp)
                            .error(R.drawable.ic_account_circle_grey_500_24dp)
                            .into(holder.itemView.img_profile)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                l_initiator.text = "${cdata.contact?.firstname}"
                if (cdata.contact?.user_positions?.size ?: -1 > 0) {
                    val time = Calendar.getInstance().timeInMillis - (cdata.contact?.user_positions?.first()?.updated_at?.time
                            ?: 0)
                    l_status.text = ExtActivity.getHoursFromLong(time)
                    l_meetdate.text = "Last update : ${cdata.contact?.user_positions?.first()?.location_name
                            ?: "-"}"
                } else {
                    l_status.text = "-"
                    l_meetdate.text = "-"

                }
                if (selAdapter.data.contains(cdata)) {
                    holder.itemView.overlay_disabler.visibility = View.VISIBLE
                } else {
                    holder.itemView.overlay_disabler.visibility = View.GONE
                }
            }
            holder.itemView.setOnClickListener {
                //add to selected
                if (selAdapter.data.contains(cdata)) return@setOnClickListener
                selAdapter.data.add(cdata)
                recycler_contactitem.post {
                    notifyItemChanged(position)
                }

                recycler_selectedcontact.post {
                    selAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    inner class SelectedContactAdapter : RecyclerView.Adapter<Holder>() {
        val data = mutableListOf<ApiData.Contact>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_contact_selected, parent, false)
            return Holder(v)
        }

        override fun getItemCount(): Int = data.size

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val cdata = data[position]
            try {
                Picasso.get().load(cdata.contact?.photo_url
                        ?: "").placeholder(R.drawable.ic_account_circle_grey_300_24dp)
                        .error(R.drawable.ic_account_circle_grey_500_24dp)
                        .into(holder.itemView.photo)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            holder.itemView.setOnClickListener {
                selAdapter.data.remove(cdata)
                recycler_contactitem.post {
                    adapter.notifyItemChanged(adapter.data.indexOf(cdata))
                }

                recycler_selectedcontact.post {
                    selAdapter.notifyDataSetChanged()
                }
            }
        }
    }
}
