package com.exairie.meet

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_create_tour_destination.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ActivityCreateTourDestination : ExtActivity(R.layout.activity_create_tour_destination), OnMapReadyCallback {


    var savedLongitude: Double = 0.0
    var savedLatitude: Double = 0.0
    var geoFences = mutableListOf<Array<Double>>()
    var map: GoogleMap? = null
    var centerMarker: Marker? = null
    var positionPolygon: Polygon? = null

    override fun onMapReady(p0: GoogleMap?) {
        this.map = p0

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        title = "Create Tour Destination"

        layout_geofence_added.visibility = View.GONE

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map_create_dest) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btn_set_geofence.setOnClickListener {
            val i = Intent(this, ActivityCreateGeofence::class.java)
            startActivityForResult(i, 1)
        }
        btn_reset_geofence.setOnClickListener {
            AlertDialog.Builder(this)
                    .setTitle("Clear geofence?")
                    .setMessage("Previous geofence data will be discarded!")
                    .setPositiveButton("Yes") { d, _ ->

                        layout_geofence_added.visibility = View.GONE
                        layout_geofence_needed.visibility = View.VISIBLE

                        centerMarker?.remove()
                        positionPolygon?.remove()

                        centerMarker = null
                        positionPolygon = null

                        geoFences.clear()
                    }
                    .setNegativeButton("No") { dialog, _ -> dialog.dismiss() }
                    .show()
        }
    }

    fun proceedCreateGroup() {
        val progress = ProgressDialog(this)
        progress.setMessage("Registering your tour destinations")
        progress.setTitle("Please wait")
        progress.show()

        val data = ApiData.TourDestination(
                creator = getUserData()?.id ?: -1,
                latitude = savedLatitude,
                longitude = savedLongitude,
                radius = 0.0,
                geofences = ApiClient.getGson().toJson(geoFences),
                createdAt = Calendar.getInstance().time,
                updatedAt = Calendar.getInstance().time,
                destinationName = text_destination_name.text.toString(),
                id = -1,
                userCreator = null,
                description = text_destination_desc.text.toString()

        )
        ApiClient.getApi().createTourDestination(data).enqueue(object : Callback<ApiData.TourDestination> {
            override fun onFailure(call: Call<ApiData.TourDestination>, t: Throwable) {
                runOnUiThread {
                    progress.dismiss()
                }
            }

            override fun onResponse(call: Call<ApiData.TourDestination>, response: Response<ApiData.TourDestination>) {
                runOnUiThread {
                    progress.dismiss()
                }
                Toast.makeText(this@ActivityCreateTourDestination, "Tour Destination ${response.body()?.destinationName} Created!", Toast.LENGTH_LONG).show()
                setResult(1)
                finish()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_meeting, menu)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1) {
            remapGeofenceResult(resultCode, data)
        }
    }

    private fun remapGeofenceResult(resultCode: Int, data: Intent?) {
        if (resultCode != 100) return
        data?.let {
            val geofenceJson = data.getStringExtra("geofences")
            val centerPointJson = data.getStringExtra("center")

            val centerPoint = ApiClient.getGson().fromJson(centerPointJson, ActivityCreateGeofence.LatLngPos::class.java)
            val geofences = ApiClient.getGson().fromJson(geofenceJson, Array<ActivityCreateGeofence.LatLngPos>::class.java)

            this.geoFences.clear()
            geofences.forEach {
                val arr = arrayOf(it.latitude, it.longitude)
                this.geoFences.add(arr)
            }

            savedLongitude = centerPoint.longitude
            savedLatitude = centerPoint.latitude

            l_geofence_info?.text = "Geofences set with ${this.geoFences.size} points. Center position is at ${String.format("%.4f", savedLatitude)},${String.format("%.4f", savedLongitude)}"

            layout_geofence_added.visibility = View.VISIBLE
            layout_geofence_needed.visibility = View.GONE

            centerMarker?.remove()
            positionPolygon?.remove()

            map?.let { map ->
                centerMarker = map.addMarker(MarkerOptions()
                        .position(LatLng(savedLatitude, savedLongitude)))
                val polygonOptions = PolygonOptions()
                        .fillColor(resources.getColor(R.color.polygonInside))
                        .strokeColor(resources.getColor(R.color.polygonBorder))
                val bounds = LatLngBounds.builder()

                geofences.forEach { pos ->
                    val l = LatLng(pos.latitude, pos.longitude)
                    polygonOptions.add(l)
                    bounds.include(l)
                }

                positionPolygon = map.addPolygon(polygonOptions)

                map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 10))
            }
        }
    }


    fun validateInput() {
        if (savedLatitude == 0.0 || savedLongitude == 0.0 || geoFences.size < 1) {
            Toast.makeText(this@ActivityCreateTourDestination, "Destination point has to be selected in map!", Toast.LENGTH_LONG).show()
            return
        }
        if (geoFences.size < 3) {
            Toast.makeText(this@ActivityCreateTourDestination, "Geofence should be consisting of minimum 3 points! Please clear and re-set your geofence", Toast.LENGTH_LONG).show()
            return
        }
        if (text_destination_desc.length() < 10) {
            Toast.makeText(this@ActivityCreateTourDestination, "Put a minimum of 10 characted to tour destination description!", Toast.LENGTH_LONG).show()
            return
        }
        if (text_destination_desc.length() < 5) {
            Toast.makeText(this@ActivityCreateTourDestination, "Put a minimum 5 character to tour destination name!", Toast.LENGTH_LONG).show()
            return
        }

        proceedCreateGroup()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mn_create -> {
                validateInput()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
