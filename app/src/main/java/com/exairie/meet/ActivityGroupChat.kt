package com.exairie.meet

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.*
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import kotlinx.android.synthetic.main.activity_group_chat.*
import kotlinx.android.synthetic.main.chat_layout.view.*
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.concurrent.timer

class ActivityGroupChat : ExtActivity(R.layout.activity_group_chat) {
    val data = mutableListOf<ApiData.GroupChat>()
    val adapter = Adapter()
    var groupInfo: ApiData.Group? = null
    val chatUpdateListener = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let { it ->
                val chatJson = it.getStringExtra("chat")
                val chat = ApiClient.getGson().fromJson(chatJson, ApiData.GroupChat::class.java)

                if (data.none { c -> c.id == chat.id } && groupInfo?.id == chat.groupId)
                    data.add(chat)

                data.sortByDescending { d -> d.createdAt }
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.mn_chatmenu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mn_gotomap -> {
                val i = Intent(this, ActivityInMeeting::class.java)
                i.putExtra("group", ApiClient.getGson().toJson(groupInfo))
                startActivity(i)
            }
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            val groupInfoJson = intent?.getStringExtra("group_info")
            groupInfo = ApiClient.getGson().fromJson(groupInfoJson, ApiData.Group::class.java)
        } catch (e: Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            finish()
        }

        setSupportActionBar(toolbar)

        title = groupInfo?.groupName
        toolbar.subtitle = "Group chat"

        registerReceiver(chatUpdateListener, IntentFilter("MEET_GROUPCHAT_UPDATE"))

        recycler_chat.adapter = adapter
        recycler_chat.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)

        groupchat_swipe_refresh.setOnRefreshListener {
            initChatInfo()
        }
        btn_send_chat.setOnClickListener {
            sendChat()
        }

        initChatInfo()

        timer("chat_refresh", false, 0, 10000) {
            initChatInfo()
        }

    }

    private fun sendChat() {
        val chatInfo = ApiData.GroupChat(
                chatText = t_chat_content.text.toString(),
                groupId = groupInfo?.id ?: -1,
                userId = getUserData()?.id ?: -1,
                id = null,
                createdAt = null,
                updatedAt = null,
                user = null
        )

        progress_send_chat.visibility = View.VISIBLE
        btn_send_chat.visibility = View.INVISIBLE

        ApiClient.getApi().sendGroupChat(chatInfo).enqueue(object : Callback<ApiData.GroupChat> {
            override fun onFailure(call: Call<ApiData.GroupChat>, t: Throwable) {
                runOnUiThread {
                    progress_send_chat?.visibility = View.GONE
                    btn_send_chat?.visibility = View.VISIBLE
                }
            }

            override fun onResponse(call: Call<ApiData.GroupChat>, response: Response<ApiData.GroupChat>) {
                runOnUiThread {
                    progress_send_chat?.visibility = View.GONE
                    btn_send_chat?.visibility = View.VISIBLE
                }
                if (response.code() == 200 && response.body() != null) {
                    val chatData = ApiData.ChatEmitData(response.body()?.groupId
                            ?: -1, response.body()!!)
                    val i = Intent("MEET_EMIT_DATA")
                    i.putExtra("event", "send_message_broadcast")
                    i.putExtra("data", ApiClient.getGson().toJson(chatData))
                    sendBroadcast(i)


                    data.add(response.body()!!)
                    data.sortByDescending { d -> d.createdAt }
                    adapter.notifyDataSetChanged()
                } else {
                    response.errorBody()?.let {
                        Log.d("ERR", it.string())
                    }
                }
            }
        })
    }

    private fun initChatInfo() {
        runOnUiThread { groupchat_swipe_refresh.isRefreshing = true }
        ApiClient.getApi().getGroupChats(groupInfo?.id
                ?: -1).enqueue(object : Callback<Array<ApiData.GroupChat>> {
            override fun onFailure(call: Call<Array<ApiData.GroupChat>>, t: Throwable) {
                runOnUiThread {
                    groupchat_swipe_refresh.isRefreshing = false
                }
            }

            override fun onResponse(call: Call<Array<ApiData.GroupChat>>, response: Response<Array<ApiData.GroupChat>>) {
                runOnUiThread {
                    groupchat_swipe_refresh.isRefreshing = false
                }
                if (response.code() == 200) {
                    response.body()?.let {
                        for (chat in it) {
                            if (data.none { c -> c.id == chat.id }) {
                                data.add(chat)
                            }
                        }
                        data.sortByDescending { d -> d.createdAt }
                        adapter.notifyDataSetChanged()
                    }
                }
            }
        })
    }

    inner class Adapter : RecyclerView.Adapter<Holder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.chat_layout, parent, false)
            return Holder(v)
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val container = holder.itemView.chat_content_container
            val bubble = holder.itemView.chatBubble
            val d = data[position]
            /** If chat sender is the same as user's id, use right bubble */
            if (d.userId == getUserData()?.id) {
                container.gravity = Gravity.RIGHT
                bubble.setBackgroundResource(R.drawable.chatbub2)
            } else {
                container.gravity = Gravity.LEFT
                bubble.setBackgroundResource(R.drawable.chatbub3)
            }

            holder.itemView.chat_sender_in.text = d.user?.firstname ?: "user ${d.userId}"
            holder.itemView.chat_content_in.text = d.chatText
            holder.itemView.chat_time_in.text = DateFormat.format("HH:mm", d.createdAt)


        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view)

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(chatUpdateListener)
    }
}
