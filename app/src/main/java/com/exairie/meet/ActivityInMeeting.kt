package com.exairie.meet

import android.app.ActionBar
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Camera
import android.graphics.Canvas
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_in_meeting.*
import kotlinx.android.synthetic.main.marker_user_layout.view.*
import kotlinx.android.synthetic.main.view_meeting_member.view.*
import kotlinx.coroutines.experimental.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlinx.coroutines.experimental.android.UI
import kotlin.concurrent.timer

class ActivityInMeeting : ExtActivity(R.layout.activity_in_meeting), OnMapReadyCallback {
    /**
     * Quick Note : Nanti InMeeting akan diganti sama keadaan tracking
     * Jadi waypoint atau titik meeting bisa di set tergantung sama si tour guide
     * dan titik meeting ngambil dari Tour Destinations
     *
     * Member meeting akan dynamic ngikut dari tour member. Jadi shortcutnya ketika
     * ada tour meeting, maka semua anggota tour member akan masuk jadi member meeting
     *
     * Adapun screen inMeeting dapat dibuka tanpa harus ada tour meeting.
     *
     * Keadaannya jika yang di screen ini adalah seorang guide, maka setiap
     * member dari meeting bisa terlihat, jika bukan guide maka hanya dia saja
     * dan titik kumpulnya
     */
    lateinit var map: GoogleMap
    lateinit var mapFragment: SupportMapFragment
    lateinit var locationManager: LocationManager

    //    lateinit var targetMarker: Marker
    lateinit var groupData: ApiData.Group

    val markerlist = mutableListOf<Marker>()
    val arrivedList = mutableListOf<ApiData.LoginData>()
    lateinit var timerChecker: Timer
    private fun getBitmap(drawableRes: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(this, drawableRes)
        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(drawable!!.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888)
        canvas.setBitmap(bitmap)
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight())
        drawable.draw(canvas)

        return bitmap
    }

    override fun onMapReady(p0: GoogleMap?) {
        map = p0!!

//        targetMarker = map.addMarker(MarkerOptions()
//                .title("Meeting Point")
//                .flat(false)
//                .position(LatLng(meeting.lat, meeting.lng))
//                .icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.meeting_point_emblem))))

//        val nPos = CameraPosition.Builder()
//                .target(targetMarker.position)
//                .zoom(20f)
//                .build()
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(nPos))

//        adapter.members = meeting.meeting_members?.map { p -> p.user!! }?.toTypedArray()
//                ?: arrayOf()

        recycler_meetingmember.layoutManager = LinearLayoutManager(this)
        recycler_meetingmember.isNestedScrollingEnabled = false
        recycler_meetingmember.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_in_meeting, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                this.finish();
            }
            R.id.mn_add_waypoint -> {

            }
        }
        return false
    }

    fun repostionMap() {
//        if(markerlist.size <= 1){
//            val pos = CameraPosition.Builder()
//                    .target(markerlist[0].position)
//                    .zoom(19f)
//                    .build()
//            map.animateCamera(CameraUpdateFactory.newCameraPosition(pos))
////            map.animateCamera(CameraUpdateFactory.zoomBy(30f))
//            return
//        }
        val bounds = LatLngBounds.Builder()
//        bounds.include(targetMarker.position)
        for (m in markerlist) {
            bounds.include(m.position)
        }
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 300))
    }

    val adapter: Adapter = Adapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        val groupStr = intent.getStringExtra("group")

        if (groupStr == null) {
            Toast.makeText(this, "Invalid data", Toast.LENGTH_LONG).show()
            finish()
            return
        }

        try {
            groupData = ApiClient.getGson().fromJson(groupStr, ApiData.Group::class.java)

        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "Incalid data", Toast.LENGTH_LONG).show()
            finish()
        }

        title = groupData.groupName
        toolbar.subtitle = "Map view"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        mapFragment.getMapAsync(this)

        timerChecker = timer("groupdata_refresh", false, 0, 30000) {
            updateGroupData();
        }
        updateGroupData();
//        timerChecker = timer("meeting_state_checker", false, 0, 10000, {
//            checkMeetingStatus()
//        })

    }

    fun updateGroupData() {
        runOnUiThread {
            layout_loading_member?.visibility = View.VISIBLE
        }
        ApiClient.getApi().getGroupInfo(groupData.id).enqueue(object : Callback<ApiData.Group> {
            override fun onFailure(call: Call<ApiData.Group>, t: Throwable) {
                runOnUiThread {
                    layout_loading_member?.visibility = View.GONE
                }
            }

            override fun onResponse(call: Call<ApiData.Group>, response: Response<ApiData.Group>) {
                runOnUiThread {
                    layout_loading_member?.visibility = View.GONE
                }

                if (response.code() == 200) {
                    response.body()?.let {
                        groupData = it
                        groupData.tour_group_members?.let { members ->
                            adapter.members.clear()
                            for (m in members) {
                                m.user?.let { user -> adapter.members.add(user) }
                            }
                        }

                        adapter.notifyDataSetChanged()

                        updateMemberLocations()
                    }
                }
            }
        })
    }

    fun updateMemberLocations() {
        val userIds = mutableListOf<Long>()
        runOnUiThread {
            layout_loading_member?.visibility = View.VISIBLE
        }
        groupData.tour_group_members?.let {
            for (member in it) {
                userIds.add(member.userId)
            }
        }

        ApiClient.getApi().getPositions(ApiData.PositionQuery(userIds.toTypedArray())).enqueue(object : Callback<Array<ApiData.Position>> {
            override fun onFailure(call: Call<Array<ApiData.Position>>, t: Throwable) {
                runOnUiThread {
                    layout_loading_member?.visibility = View.GONE
                }
            }

            override fun onResponse(call: Call<Array<ApiData.Position>>, response: Response<Array<ApiData.Position>>) {
                runOnUiThread {
                    layout_loading_member?.visibility = View.GONE
                }

                if (response.code() == 200) {
                    runOnUiThread {
                        response.body()?.let {
                            updateMapMarkerPositions(it)
                        }
                    }
                } else {
                    response.errorBody()?.let {
                        Log.d("Err", it.string())
                    }
                }
            }
        })
    }

    fun updateMapMarkerPositions(positions: Array<ApiData.Position>) {
        for (pos in positions) {
            var user: ApiData.LoginData? = null
            val filter = groupData.tour_group_members?.filter { d -> d.userId == pos.user_id }
            filter?.let { f ->
                if (!f.isEmpty()) {
                    user = f.first().user
                    user?.let {
                        var marker = markerlist.filter { x -> x.tag == it.id.toString() }

                        if (marker.isEmpty()) {
                            val nmarker = map.addMarker(MarkerOptions()
                                    .position(LatLng(pos.lat, pos.lng))
                                    .title(it.firstname))

                            val v = layoutInflater.inflate(R.layout.marker_user_layout, null)

                            nmarker.setIcon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this@ActivityInMeeting, v, it.photo_url
                                    ?: "")))
                            nmarker.tag = it.id.toString()
                            markerlist.add(nmarker)
                        } else {
                            marker[0].position = LatLng(pos.lat, pos.lng)
                        }
                    }
                }
            }
        }
        if (markerlist.size > 1) {
            val bounds = LatLngBounds.builder()
            markerlist.forEach { m -> bounds.include(m.position) }
            val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds.build(), 280)
            map.animateCamera(cameraUpdate)
        }
        /**
        var isMarkerInstalled = false
        for (marker in markerlist) {
        if (marker.tag == m.id.toString()) {
        marker.position = LatLng(latest!!.lat, latest.lng)
        isMarkerInstalled = true
        break
        }
        }

        if (!isMarkerInstalled) {
        val marker = map.addMarker(MarkerOptions()
        .position(LatLng(latest!!.lat, latest.lng))
        .title(m.firstname))

        val v = layoutInflater.inflate(R.layout.marker_user_layout, null)

        marker.setIcon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this@ActivityInMeeting, v, m.photo_url
        ?: "")))
        marker.tag = m.id.toString()
        markerlist.add(marker)
        }
         */
    }

//    fun checkMeetingStatus() {
//        ApiClient.getApi().getMeeting(meeting.id).enqueue(object : Callback<ApiData.Meeting> {
//            override fun onFailure(call: Call<ApiData.Meeting>?, t: Throwable?) {
//                Toast.makeText(this@ActivityInMeeting, "Unable to check meeting state", Toast.LENGTH_LONG).show()
//            }
//
//            override fun onResponse(call: Call<ApiData.Meeting>?, response: Response<ApiData.Meeting>?) {
//                if (response?.code() == 200) {
//                    if (response.body()?.status != "active") {
////                        finishMeeting()
//                        timerChecker.cancel()
//                        val i = Intent(this@ActivityInMeeting, ActivityMeetComplete::class.java)
//                        i.putExtra("id", meeting.id)
//                        startActivityForResult(i, 101)
//                    }
//                }
//            }
//        })
//    }

    inner class Adapter : RecyclerView.Adapter<Holder>() {
        var members: MutableList<ApiData.LoginData> = mutableListOf<ApiData.LoginData>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_meeting_member, parent, false)
            return Holder(v)
        }

        override fun getItemCount(): Int = members.size

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val v = holder.itemView
            val m = members[position]
            if (m.id == getUserData()?.id) {
                v.l_firstname.text = "You"
            } else {
                v.l_firstname.text = m.firstname
            }
            v.progress.max = 1000

            try {
                Picasso.get().load(m.photo_url
                        ?: "").placeholder(R.drawable.ic_account_circle_grey_300_24dp)
                        .error(R.drawable.ic_account_circle_grey_500_24dp)
                        .into(holder.itemView.user_photo)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    private fun updateArrivalUI() {
        val otw = adapter.members.size - arrivedList.size
        val arrived = arrivedList.size

//        if(arrived == 0){
//            l_arrivalstatus.text = "${otw} on the way"
//        }else if(otw == 0){
//            l_arrivalstatus.text = "Everyone arrived"
//        }else{
//            l_arrivalstatus.text = "${otw} on the way, ${arrived} arrived"
//        }

        if (otw == 0 && arrived == adapter.members.size) {
            val i = Intent(this, ActivityMeetComplete::class.java)
//            i.putExtra("id", meeting.id)
            startActivityForResult(i, 101)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            101 -> {
                if (resultCode == 1) {
                    //Finish meeting!
                    finishMeeting()
                }
            }
        }
    }

    private fun finishMeeting() {
    }

    fun calculateSpeed(track1: ApiData.Position, track2: ApiData.Position): Float {
        val deltaTime = Math.abs(track2.created_at.time - track1.created_at.time) / 1000 //in secs

        val t1 = Location("")
        t1.latitude = track1.lat
        t1.longitude = track1.lng

        val t2 = Location("")
        t2.latitude = track2.lat
        t2.longitude = track2.lng

        val distance = t1.distanceTo(t2)

        return distance / deltaTime
    }

    fun createDrawableFromView(context: Context, view: View, imgUrl: String): Bitmap {
        try {
            Picasso.get().load(imgUrl).placeholder(R.drawable.ic_account_circle_grey_300_24dp)
                    .error(R.drawable.ic_account_circle_grey_500_24dp)
                    .into(view.photo)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val displayMetrics = DisplayMetrics()
        (context as Activity).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        view.setLayoutParams(ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT))
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888)

        val canvas = Canvas(bitmap)
        view.draw(canvas)

        return bitmap
    }
}
