package com.exairie.meet

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import kotlinx.android.synthetic.main.activity_login.*
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ActivityLogin : ExtActivity(R.layout.activity_login) {
    lateinit var signInClient: GoogleSignInClient
    private val RC_SIGN_IN = 10
    private var imgGoogleClick = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        title = "Log In"

        btn_login.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
            finish()
        }
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(getString(R.string.default_web_client_id))
                .requestId()
                .requestEmail()
                .requestProfile()
                .build()
        signInClient = GoogleSignIn.getClient(this, gso)

        if (intent.getStringExtra("logout") != null) {
            signInClient.signOut().addOnCompleteListener(this, object : OnCompleteListener<Void> {
                override fun onComplete(p0: Task<Void>) {
                    val i = Intent(this@ActivityLogin, ActivityLogin::class.java)
                    startActivity(i)
                    finish()
                }
            })
            return
        }

        btn_login.setOnClickListener {
            login()
        }

        imggoogle.setOnClickListener {
            imgGoogleClick++
            if (imgGoogleClick >= 8) {
                val fakeLoginData = ApiData.LoginData(
                        google_auth_code = "0000000",
                        firstname = "EMULATOR",
                        lastname = "001",
                        email = "dummy@worldisinheaven.com",
                        phone = "0001",
                        token = "abc123",
                        photo_url = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.liputan6.com%2Fshowbiz%2Fread%2F4038478%2F5-fakta-tak-disangka-tentang-mia-khalifa-mantan-bintang-film-panas&psig=AOvVaw3XEtJzRiDKqSanHkfXQkMO&ust=1587301636173000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNDx2teF8ugCFQAAAAAdAAAAABAD",
                        is_guide = 0
                )

                sendLogin(fakeLoginData)
            }
        }
    }


    fun login() {
        val signInIntent = signInClient.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                val regData = ApiData.LoginData(account.id, account.displayName, account.familyName, account.email, "-", "-", photo_url = account.photoUrl.toString(), is_guide = 0)
                sendLogin(regData)
//                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                e.printStackTrace()
//                Log.w(FragmentActivity.TAG, "Google sign in failed", e)
                // ...
            }

        }
    }

    fun sendLogin(data: ApiData.LoginData) {
        val dialog = ProgressDialog(this)
        dialog.setMessage("Please wait")
        dialog.setCancelable(false)
        runOnUiThread { dialog.show() }
        ApiClient.getApi().registerUserAccount(data).enqueue(object : Callback<ApiData.LoginData?> {
            override fun onFailure(call: Call<ApiData.LoginData?>?, t: Throwable?) {
//                Toast.makeText(this@ActivityLogin,"Unable to process your login information!",Toast.LENGTH_LONG).show()
                runOnUiThread {
                    AlertDialog.Builder(this@ActivityLogin)
                            .setTitle("Login Error")
                            .setMessage("Unable to process your login information")
                            .setPositiveButton("Retry") { o, _ ->
                                o.dismiss()
                                sendLogin(data)
                            }
                            .show()
                }
                runOnUiThread { dialog.dismiss() }
            }

            override fun onResponse(call: Call<ApiData.LoginData?>?, response: Response<ApiData.LoginData?>?) {
                if (response?.code() == 200) {
                    setUserData(response.body())
                } else {
                    runOnUiThread {
                        AlertDialog.Builder(this@ActivityLogin)
                                .setTitle("Login Error")
                                .setMessage("Unable to save your login information")
                                .setPositiveButton("Retry", { o, _ ->
                                    o.dismiss()
                                    sendLogin(data)
                                })
                                .show()
                    }
                }
                Log.d("Error", response?.errorBody()?.string() + "")
                runOnUiThread { dialog.dismiss() }

                if (getUserData() != null) {
                    val i = Intent(this@ActivityLogin, MainActivity::class.java)
                    startActivity(i)
                    finish()
                    return
                }
            }
        })
    }
}
