package com.exairie.meet

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_meet_complete.*

class ActivityMeetComplete : ExtActivity(R.layout.activity_meet_complete) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setResult(0)
        btn_finish.setOnClickListener {
            setResult(1)
            finish()
        }
    }
}
