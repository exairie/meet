package com.exairie.meet

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData

import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_contactitem.view.*
import kotlinx.android.synthetic.main.fragment_contactitem_container.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ContactFragment.OnListFragmentInteractionListener] interface.
 */
class ContactFragment : Fragment() {

    //    data class ContactData(val name : String, val lastseen : String, val date : Date)
    val adapter = Adapter()
    lateinit var pref: SharedPreferences
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_contactitem_container, null)

        return v
    }

    fun getUserData(): ApiData.LoginData? {
        val udata = pref.getString("userdata", "null")
        if (udata == "null") return null
        return ApiClient.getGson().fromJson(udata, ApiData.LoginData::class.java)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            101 -> {
                if (resultCode == 1) {
                    loadContacts()
                }
            }
        }
    }

    fun setUserData(data: ApiData.LoginData?) {
        if (data != null) {
            pref.edit()
                    .putString("userdata", ApiClient.getGson().toJson(data))
                    .apply()
        } else {
            pref.edit()
                    .remove("userdata")
                    .apply()
        }
    }

    fun loadContacts() {
        refresher.isRefreshing = true
        ApiClient.getApi().getContacts(getUserData()?.id
                ?: -1).enqueue(object : Callback<Array<ApiData.Contact>> {
            override fun onFailure(call: Call<Array<ApiData.Contact>>?, t: Throwable?) {
                activity?.runOnUiThread {
                    context?.let {
                        AlertDialog.Builder(context!!)
                                .setTitle("Loading Error")
                                .setMessage("Unable get your contacts")
                                .setPositiveButton("Retry") { o, _ ->
                                    o.dismiss()
                                    loadContacts()
                                }
                                .show()
                    }
                }
            }

            override fun onResponse(call: Call<Array<ApiData.Contact>>?, response: Response<Array<ApiData.Contact>>?) {

                activity?.runOnUiThread {
                    refresher?.isRefreshing = false
                }
                if (response?.code() == 200) {
                    adapter.data = mutableListOf()
                    for (i in response.body() ?: arrayOf()) {
                        adapter.data.add(i)
                    }
                    activity?.runOnUiThread {
                        l_no_contacts?.visibility = if (adapter.data.size < 1) View.VISIBLE else View.GONE
                    }
                    recycler_contactitem?.post {
                        adapter.notifyDataSetChanged()
                    }
                } else {
                    activity?.runOnUiThread {
                        context?.let {
                            AlertDialog.Builder(context!!)
                                    .setTitle("Loading Error")
                                    .setMessage("Unable fetch your contacts")
                                    .setPositiveButton("Retry") { o, _ ->
                                        o.dismiss()
                                        loadContacts()
                                    }
                                    .show()
                        }
                    }
                }
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pref = activity!!.getSharedPreferences(context!!.packageName, Context.MODE_PRIVATE)
        val recycler = recycler_contactitem

        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(context)
//        val calendar = Calendar.getInstance()
//        calendar.add(Calendar.MINUTE,3)
//        adapter.data.add(ContactData("Maryam Susanti","Pondok Gede", calendar.time))
//        calendar.add(Calendar.MINUTE,5)
//        adapter.data.add(ContactData("Nuraini Azizah","Cawang", calendar.time))
//        calendar.add(Calendar.MINUTE,7)
//        adapter.data.add(ContactData("Aditya Pratama","Sragen", calendar.time))
//        calendar.add(Calendar.MINUTE,8)
//        adapter.data.add(ContactData("Fathiah","Ciomas", calendar.time))
//        calendar.add(Calendar.MINUTE,8)
//        adapter.data.add(ContactData("Ridwan Achadi","Cianjur", calendar.time))
//        calendar.add(Calendar.MINUTE,9)
//        adapter.data.add(ContactData("Setya Achsanul","Kelapa Sawit", calendar.time))
//        calendar.add(Calendar.MINUTE,12)
//        adapter.data.add(ContactData("Juki","Pisangan", calendar.time))
//        calendar.add(Calendar.MINUTE,13)
//        adapter.data.add(ContactData("Alan Marbutoh","Rawamangun", calendar.time))
//        calendar.add(Calendar.MINUTE,15)
//        adapter.data.add(ContactData("Juminten","Karawang", calendar.time))

        recycler.post({
            adapter.notifyDataSetChanged()
        })

        loadContacts()
        refresher.setOnRefreshListener { loadContacts() }

    }

    inner class Adapter : RecyclerView.Adapter<Holder>() {
        var data = mutableListOf<ApiData.Contact>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.fragment_contactitem, parent, false)
            return Holder(v)
        }

        override fun getItemCount(): Int = data.size

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val cdata = data[position]
            with(holder.itemView) {
                try {
                    Picasso.get().load(cdata.contact?.photo_url
                            ?: "").placeholder(R.drawable.ic_account_circle_grey_300_24dp)
                            .error(R.drawable.ic_account_circle_grey_500_24dp)
                            .into(holder.itemView.img_profile)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                l_initiator.text = "${cdata.contact?.firstname}"
                if (cdata.contact?.user_positions?.size ?: -1 > 0) {
                    val time = Calendar.getInstance().timeInMillis - (cdata.contact?.user_positions?.first()?.updated_at?.time
                            ?: 0)
                    l_status.text = ExtActivity.getHoursFromLong(time)
                    l_meetdate.text = "Last update : ${cdata.contact?.user_positions?.first()?.location_name
                            ?: "-"}"
                } else {
                    l_status.text = "-"
                    l_meetdate.text = "-"

                }

                setOnClickListener {
                    AlertDialog.Builder(context)
                            .setTitle("Create a meeting")
                            .setMessage("You want to create a meeting with ${cdata.contact?.firstname}?")
                            .setPositiveButton("Yes") { d, _ ->
                                d.dismiss()
                                val i = Intent(context, ActivityCreateMeeting::class.java)
                                i.putExtra("users", ApiClient.getGson().toJson(arrayOf(cdata.contact)))
                                startActivity(i)
                            }
                            .setNegativeButton("No", { d, _ -> d.dismiss() })
                            .show()
                }
            }

        }
    }
}
