package com.exairie.meet

import android.content.*
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import java.util.jar.Manifest

open class ExtActivity(viewid : Int) : AppCompatActivity(){
    val viewid : Int
    lateinit var pref : SharedPreferences
    init {
        this.viewid = viewid
    }

    companion object {
        fun getHoursFromLong(`val`: Long): String {
            var `val` = `val`
            val h = `val` / (1000 * 60 * 60)

            `val` -= h * (1000 * 60 * 60)

            val m = `val` / (1000 * 60)

            `val` -= m * (1000 * 60)

            val s = `val` / 1000

            `val` -= s * 1000

//        Log.d("MILIS", `val`.toString())
            var time = ""
            if(h >= 1){
                return "${h} Hour(s)"
            }
            time += if (h < 10) "0" + h.toString() else h.toString()
            time += ":"
            time += if (m < 10) "0" + m.toString() else m.toString()
            time += ":"
            time += if (s < 10) "0" + s.toString() else s.toString()
//        Log.d("TIME", time)
            return time
        }
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(viewid)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(viewid)
        pref = getSharedPreferences(packageName, Context.MODE_PRIVATE)

    }


    protected fun hasBackButton(istrue: Boolean) {
        try {
            supportActionBar!!.setDisplayHomeAsUpEnabled(istrue)
            supportActionBar!!.setHomeButtonEnabled(istrue)
        } catch (e: Exception) {
            e.printStackTrace()

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }
    fun getUserData() : ApiData.LoginData?{
        val udata = pref.getString("userdata","null")
        if(udata == "null") return null
        return ApiClient.getGson().fromJson(udata, ApiData.LoginData::class.java)
    }

    fun setUserData(data : ApiData.LoginData?){
        if(data != null){
            pref.edit()
                    .putString("userdata",ApiClient.getGson().toJson(data))
                    .apply()
        }else{
            pref.edit()
                    .remove("userdata")
                    .apply()
        }
    }

    fun setInMeeting(m : ApiData.Meeting?){
        if(m != null){
            with(pref.edit()){
                putString("inmeeting",ApiClient.getGson().toJson(m))
                apply()
            }
        }else{
            with(pref.edit()){
                remove("inmeeting")
                apply()
            }
        }
    }
    fun getMeetingState() : ApiData.Meeting?{
        val m = pref.getString("inmeeting","null")
        if(m == "null"){
            return null
        }else{
            return ApiClient.getGson().fromJson(m,ApiData.Meeting::class.java)
        }
    }
}