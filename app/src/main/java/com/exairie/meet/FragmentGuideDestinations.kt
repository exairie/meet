package com.exairie.meet


import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.fragment_guide_destinations.*
import kotlinx.android.synthetic.main.view_tour_destination.view.*
import kotlinx.android.synthetic.main.view_tour_group_itemlist.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentGuideDestinations.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentGuideDestinations : Fragment() {
    // TODO: Rename and change types of parameters
    val destinations = mutableListOf<ApiData.TourDestination>()
    val adapter = TourDestinationAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recycler_guide_destinations.adapter = adapter
        recycler_guide_destinations.layoutManager = LinearLayoutManager(context)

        layout_nodata.visibility = View.VISIBLE
        recycler_guide_destinations.visibility = View.GONE

        destination_swipe_refresh.setOnRefreshListener {
            loadData()
        }

        loadData()
    }

    fun loadData() {
        destination_swipe_refresh?.isRefreshing = true
        context?.let { context ->
            ApiClient.getApi().getGuideDestination(ApiClient.getLoggedInUserData(context)?.id ?: -1)
                    .enqueue(object : Callback<Array<ApiData.TourDestination>> {
                        override fun onFailure(call: Call<Array<ApiData.TourDestination>>, t: Throwable) {
                            destination_swipe_refresh?.post {
                                destination_swipe_refresh?.isRefreshing = false
                            }
                        }

                        override fun onResponse(call: Call<Array<ApiData.TourDestination>>, response: Response<Array<ApiData.TourDestination>>) {
                            destination_swipe_refresh?.post {
                                destination_swipe_refresh?.isRefreshing = false
                            }
                            if (response.code() == 200) {
                                response.body()?.let {
                                    destinations.clear()
                                    destinations.addAll(it)
                                    adapter.notifyDataSetChanged()

                                    layout_nodata.visibility = if(it.isNotEmpty())View.GONE else View.VISIBLE
                                    recycler_guide_destinations.visibility = if(it.isEmpty())View.GONE else View.VISIBLE
                                }
                            }
                        }
                    })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guide_destinations, container, false)
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {

    }

    inner class TourDestinationAdapter : RecyclerView.Adapter<Holder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.view_tour_destination, parent, false)
            return Holder(view)
        }

        override fun getItemCount(): Int {
            return destinations.size
        }

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: Holder, position: Int) {
            val d = destinations[position]
            holder.itemView.dest_name.text = d.destinationName
            holder.itemView.dest_desc.text = d.description

            LocationSender.lastLocationTracked?.let {
                val locTarget = Location("MATH")
                locTarget.latitude = d.latitude
                locTarget.longitude = d.longitude
                val distance = it.distanceTo(locTarget)
                if (distance > 1000) {
                    holder.itemView.dest_desc.text = "${String.format("%.2f", distance / 1000)}km away"
                } else {
                    holder.itemView.dest_desc.text = "${distance}meters away"
                }
            }

            val center = ActivityCreateGeofence.LatLngPos(d.latitude,d.longitude)

            holder.itemView.setOnClickListener {
                val i = Intent(context,ActivityCreateGeofence::class.java)
                i.putExtra("readonly",true)
                i.putExtra("center",ApiClient.getGson().toJson(center))
                i.putExtra("geofences",d.geofences)
                i.putExtra("destination_name",d.destinationName)

                context?.startActivity(i)
            }
        }

    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentGuideDestinations.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentGuideDestinations().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
