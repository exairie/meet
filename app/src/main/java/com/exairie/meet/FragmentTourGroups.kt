package com.exairie.meet


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import kotlinx.android.synthetic.main.fragment_tour_groups.*
import kotlinx.android.synthetic.main.view_tour_group_itemlist.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.util.*
import java.util.concurrent.TimeUnit


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentTourGroups.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentTourGroups : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var loggedUserData: ApiData.LoginData? = null
    val groups = mutableListOf<ApiData.Group>()
    val adapter = TourGroupAdapter()
    val now = Calendar.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        context?.let {
            loggedUserData = ApiClient.getLoggedInUserData(it)
        }
    }

    fun reloadGroupData() {
        group_swipe_refresh?.isRefreshing = true
        context?.let {
            ApiClient.getApi().getUserTourGroups(ApiClient.getLoggedInUserData(context!!)?.id
                    ?: -1).enqueue(object : Callback<Array<ApiData.Group>> {
                override fun onFailure(call: Call<Array<ApiData.Group>>, t: Throwable) {
                    Toast.makeText(context, "Unable to load tour groups : " + t.message, Toast.LENGTH_LONG).show()
                    group_swipe_refresh?.isRefreshing = false
                }

                override fun onResponse(call: Call<Array<ApiData.Group>>, response: Response<Array<ApiData.Group>>) {
                    group_swipe_refresh?.isRefreshing = false
                    groups.clear()
                    groups.addAll(response.body() as Array<out ApiData.Group>)

                    adapter.notifyDataSetChanged()
                }
            })

        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recycler_tourgroup.adapter = adapter
        recycler_tourgroup.layoutManager = LinearLayoutManager(context)

        group_swipe_refresh.setOnRefreshListener {
            reloadGroupData()
        }

        reloadGroupData()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tour_groups, container, false)
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {

    }

    inner class TourGroupAdapter : RecyclerView.Adapter<Holder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.view_tour_group_itemlist, parent,false)
            return Holder(view)
        }

        override fun getItemCount(): Int {
            return groups.size
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val data = groups[position]
            var groupCreationDate = android.text.format.DateFormat.format("dd MMM HH:mm", data.createdAt)
            val diff = now.timeInMillis - data.createdAt.time

            if(TimeUnit.DAYS.toDays(diff) == 1L){
                groupCreationDate = " yesterday "
            }
            if(TimeUnit.DAYS.toDays(diff) == 2L){
                groupCreationDate = " two days ago "
            }
            if(TimeUnit.DAYS.toDays(diff) in 7..13){
                groupCreationDate = " last week "
            }
            if(TimeUnit.DAYS.toDays(diff) in 30..59){
                groupCreationDate = " last month "
            }

            holder.itemView.group_name.text = data.groupName
            holder.itemView.group_desc.text = if (loggedUserData?.id == data.initiator) "Created at $groupCreationDate" else "Created By ${data.initiated_by?.firstname} at ${groupCreationDate}"

            holder.itemView.setOnClickListener {
                val i = Intent(context, ActivityGroupChat::class.java)
                i.putExtra("group_info",ApiClient.getGson().toJson(data))

                context?.startActivity(i)
            }
        }

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentTourGroups.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentTourGroups().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
