package com.exairie.meet


import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import kotlinx.android.synthetic.main.fragment_tour_guide_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.concurrent.timer

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentTourGuideRegister.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentTourGuideRegister : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    fun registerForTourGuide() {

        btn_register_tourguide?.visibility = View.GONE
        layout_registering?.visibility = View.VISIBLE

        val userid = ApiClient.getLoggedInUserData(context!!)?.id ?: -1
        if (userid == -1L) {
            Toast.makeText(context, "ERROR : Userid -1", Toast.LENGTH_LONG).show()
            btn_register_tourguide?.visibility = View.VISIBLE
            layout_registering?.visibility = View.GONE
            return
        }
        ApiClient.getApi().setUserAsGuide(userid).enqueue(object : Callback<ApiData.LoginData> {
            override fun onFailure(call: Call<ApiData.LoginData>, t: Throwable) {
                t.printStackTrace()
                AlertDialog.Builder(context)
                        .setTitle("Error")
                        .setMessage("We can't set your account as guide account.\nErr : ${t.message}")
                        .setNegativeButton("Dismiss") { d, _ ->
                            d.dismiss()
                        }
                        .show()
                btn_register_tourguide?.visibility = View.VISIBLE
                layout_registering?.visibility = View.GONE
            }

            override fun onResponse(call: Call<ApiData.LoginData>, response: Response<ApiData.LoginData>) {
                val userData = response.body()
                if (userData != null) {
                    context?.let {
                        ApiClient.saveLoggedUserData(it, userData)
                        val i = Intent("MEETBC_RELOAD_USERINFO")
                        it.sendBroadcast(i)
                    }

                } else {
                    AlertDialog.Builder(context)
                            .setTitle("Error")
                            .setMessage("We can't set your account as guide account.\nErr : ACC_NOCHG")
                            .setNegativeButton("Dismiss") { d, _ ->
                                d.dismiss()
                            }
                            .show()
                }
                btn_register_tourguide?.visibility = View.VISIBLE
                layout_registering?.visibility = View.GONE
            }
        })

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_register_tourguide.visibility = View.VISIBLE
        layout_registering.visibility = View.GONE


        btn_register_tourguide.setOnClickListener {
            btn_register_tourguide?.let { _ ->

                AlertDialog.Builder(context)
                        .setTitle("Confirmation")
                        .setMessage("Do you really wan't to change your account as a guide account? This action is irreversible")
                        .setPositiveButton("Yes") { d, i ->
                            registerForTourGuide()
                            d.dismiss()
                        }
                        .setNegativeButton("No") { d, i ->
                            d.dismiss()
                        }
                        .show()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tour_guide_register, container, false)
    }


    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentTourGuideRegister().apply {

                }
    }
}
