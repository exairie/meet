package com.exairie.meet

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.util.Log
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import com.github.nkzawa.socketio.client.IO
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GroupListenerService : Service() {

    private val io = IO.socket("https://meetchatsocket.herokuapp.com/")
    private val registeredGroups = mutableListOf<ApiData.Group>()
    private val emitterListener = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val event = intent?.getStringExtra("event") ?: "__NOACTION"
            val data = intent?.getStringExtra("data") ?: ""
            io?.emit(event, data)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        try {
            registerReceiver(emitterListener, IntentFilter("MEET_EMIT_DATA"))
        } catch (e: Exception) {
            e.printStackTrace()
        }

        io.connect()

        io.on("location_updates") { data ->
            try {
                val json = data[0].toString()
                val position = ApiClient.getGson().fromJson(json, ApiData.Position::class.java)
                val i = Intent("MEET_MEMBER_UPDATE")
                i.putExtra("position", json)

                sendBroadcast(i)

            } catch (e: JSONException) {

            }
        }
        io.on("chat") { data ->
            try {
                val jobj = data[0] as JSONObject
                val datastring = jobj.toString()
                val chat = ApiClient.getGson().fromJson(datastring, ApiData.GroupChat::class.java)
                val i = Intent("MEET_GROUPCHAT_UPDATE")
                i.putExtra("chat", datastring)

                sendBroadcast(i)

            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
            }
        }
        io.on("sys_notif") { data ->
            Log.d("NOTIF", data[0].toString())
        }
        io.on("disconnect") { data ->
            Log.d("SOCKET", "Disconnected. Attempting to reconnect")
            io.connect()
        }
        io.on("connect") {
            Log.d("SOCKET", "Connect");
        }

        ApiClient.getApi().getUserTourGroups(ApiClient.getLoggedInUserData(this)?.id ?: -1)
                .enqueue(object : Callback<Array<ApiData.Group>> {
                    override fun onFailure(call: Call<Array<ApiData.Group>>, t: Throwable) {
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<Array<ApiData.Group>>, response: Response<Array<ApiData.Group>>) {
                        if (response.code() == 200) {
                            response.body()?.let {
                                registeredGroups.addAll(it)
                                it.forEach { group ->
                                    io.emit("join", group.id.toString())
                                    Log.d("SOCKET", "Joining group id ${group.id}")
                                }
                            }
                        } else {
                            response.errorBody()?.let {
                                Log.d("ERRORBODY", it.string())
                            }
                        }
                    }
                })
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            unregisterReceiver(emitterListener)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }
}
