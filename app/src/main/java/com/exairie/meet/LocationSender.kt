package com.exairie.meet

import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.*
import android.location.LocationManager.GPS_PROVIDER
import android.location.LocationManager.NETWORK_PROVIDER
import android.os.AsyncTask
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.support.v4.app.NotificationCompat
import android.support.v4.app.ServiceCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import kotlinx.android.synthetic.main.activity_create_meeting.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.concurrent.timer

class LocationSender : Service() {
    var user : ApiData.LoginData? = null
    get(){
        if(field == null){
            val pref = getSharedPreferences(packageName, Context.MODE_PRIVATE)
            val udata = pref.getString("userdata","null")

            if(udata == "null") return null

            field = ApiClient.getGson().fromJson(udata, ApiData.LoginData::class.java)
        }

        return field
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val t = timer("timer_checker",false,0,10000,{
            getMeetingState()
        })

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
//            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),10)
            return START_NOT_STICKY
        }
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
//            ServiceCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),11)
            return START_NOT_STICKY
        }

        if(manager.allProviders.contains(GPS_PROVIDER)){
            manager.requestLocationUpdates(GPS_PROVIDER,15000,5f,object : LocationListener{
                override fun onLocationChanged(location: Location?) = sendLocation(location!!)

                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) = Unit

                override fun onProviderEnabled(provider: String?) = Unit

                override fun onProviderDisabled(provider: String?) = Unit
            })
        }

        return START_STICKY
    }
    fun getMeetingState() : Unit{
        val pref = getSharedPreferences(packageName,Context.MODE_PRIVATE)
        val m = pref.getString("inmeeting","null")
        if(m != "null") return

        if(user == null) return
        if(meetingChecked == true) return

        ApiClient.getApi().getLastState(user?.id ?: -1).enqueue(object : Callback<ApiData.Meeting> {
            override fun onFailure(call: Call<ApiData.Meeting>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<ApiData.Meeting>?, response: Response<ApiData.Meeting>?) {
                if(response?.code() == 200){
                    meetingChecked = true
                    if(response.body()?.status == "active"){
                        val man = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                        val i = Intent(broadcastID)
                        val meeting = ApiClient.getGson().toJson(response.body())
                        i.putExtra("meeting",meeting)

                        val notif = NotificationCompat.Builder(this@LocationSender)
                                .setSmallIcon(R.drawable.ic_arrow_shrink)
                                .setLargeIcon(BitmapFactory.decodeResource(resources,R.mipmap.ic_launcher))
                                .setContentTitle("You have a meeting")
                                .setContentText("Click here to view your meeting")
                                .setContentIntent(PendingIntent.getActivity(this@LocationSender,10,i,PendingIntent.FLAG_CANCEL_CURRENT))
                                .build()

                        sendBroadcast(i)

                        man.notify(122,notif)
                    }
                }
            }
        })
    }
    override fun onBind(intent: Intent): IBinder? = null
    fun sendLocation(loc : Location){

        if(user == null) return
        val locationName = geoCode(loc.latitude,loc.longitude)
        val positionData = ApiData.Position(-1,user!!.id,loc.latitude,loc.longitude,loc.speed,loc.bearing,locationName,Date(),Date())

        val i = Intent("MEET_LOCATION_UPDATE")
        i.putExtra("latitude",loc.latitude)
        i.putExtra("longitude",loc.longitude)
        i.putExtra("speec",loc.speed)

        val i2 = Intent("MEET_EMIT_DATA")
        i2.putExtra("event","update_location")
        i2.putExtra("data",ApiClient.getGson().toJson(positionData))

        sendBroadcast(i)
        sendBroadcast(i2)

        lastLocationTracked = loc



        ApiClient.getApi().updateLocation(positionData).enqueue(object : Callback<Any>{
            override fun onFailure(call: Call<Any>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                if(response?.code() == 201){
                    val man = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val notif = NotificationCompat.Builder(this@LocationSender)
                            .setContentTitle("Location Updater")
                            .setContentText("Your position has been updated")
                            .setSmallIcon(R.drawable.ic_info_outline_white_24dp)
                            .setLargeIcon(BitmapFactory.decodeResource(resources,R.mipmap.ic_launcher))
                            .build()
                    man.notify(10,notif)
                }
            }
        })
    }

    fun geoCode(lat : Double, lng : Double) : String{
        val context : Context? = this
        var addresses : List<Address>? = null
        val geocoder = Geocoder(context, Locale.getDefault())
        var city = ""
        try {
            addresses = geocoder.getFromLocation(lat,lng,3)

            if(addresses.size > 0){
                val address = addresses[0]
                var strAddress = mutableListOf<String>()
                if (addresses.get(0).locality != null) {
                    city = addresses[0].locality
                } else if (addresses[0].adminArea != null) {
                    city = addresses[0].adminArea
                }

                return city
            }
        } catch (e: IOException) {
            e.printStackTrace()
            return "Unknown"
        }

        return "Unknown"
    }
    companion object {
        val broadcastID = "com.exairie.meet.START_MEETING"
        var meetingChecked = false
        var lastLocationTracked : Location? = null
    }
}
