package com.exairie.meet

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.TabHost
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.jar.Manifest

class MainActivity : ExtActivity(R.layout.activity_main), ViewPager.OnPageChangeListener {
    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        val btnVisibility = if (position >= 2) View.GONE else View.VISIBLE
        val guideBtnVisibility = if (position < 2) View.GONE else View.VISIBLE
        btn_add_user.visibility = if(position == 0)View.VISIBLE else View.GONE
        btn_add_group.visibility = if(position == 1) View.VISIBLE else View.GONE


        if (getUserData()?.is_guide == 1) {
            btn_add_destination.visibility = guideBtnVisibility
        } else {
            btn_add_destination.visibility = View.GONE
        }
    }

    data class Page(val page: Fragment, val title: String)

    val reloadUserListener = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            setupScreen()
        }
    }

    val contactFragment = ContactFragment()
    lateinit var adapter: PagerAdapter
    val meetingReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent!!.setClass(this@MainActivity, ActivityInMeeting::class.java)
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkForPermission()
        checkMeetingState()
        if (getUserData() == null) {
            val i = Intent(this, ActivityLogin::class.java)
            startActivity(i)
            finish()
            return
        }
        setSupportActionBar(toolbar)
        title = "Meet"


        startService(Intent(this,GroupListenerService::class.java))


        btn_add_user.setOnClickListener {
            val i = Intent(this, ActivityAddContact::class.java)
            startActivityForResult(i, 101)
        }

        btn_add_destination.setOnClickListener {
            val i = Intent(this, ActivityCreateTourDestination::class.java)
            startActivity(i)
        }
        btn_add_group.setOnClickListener {
            val i = Intent(this, ActivityCreateGroup::class.java)
            startActivity(i)
        }
        viewpager_content.addOnPageChangeListener(this)

        setupScreen()

        try {
            registerReceiver(meetingReceiver, IntentFilter(LocationSender.broadcastID))
        } catch (e: Exception) {
        }
        try {
            registerReceiver(reloadUserListener, IntentFilter("MEETBC_RELOAD_USERINFO"))
        } catch (e: Exception) {
        }
    }

    fun setupScreen() {
        val loggedUser = getUserData()

        adapter = PagerAdapter(supportFragmentManager)

        adapter.data.add(Page(contactFragment, "Contacts"))
        adapter.data.add(Page(FragmentTourGroups(), "Groups"))

        if (loggedUser?.is_guide == 0) {
            adapter.data.add(Page(FragmentTourGuideRegister(), "Tour Guide"))
        } else if (loggedUser?.is_guide == 1) {
            adapter.data.add(Page(FragmentGuideDestinations(), "Destinations"))
        }

        btn_add_user.visibility = View.VISIBLE
        btn_add_destination.visibility = View.GONE

        btn_add_group.visibility = View.GONE



        viewpager_content.adapter = adapter
        tab.setupWithViewPager(viewpager_content)

    }

    override fun onDestroy() {
        try {
            unregisterReceiver(meetingReceiver)
        } catch (e: Exception) {
        }
        try {
            unregisterReceiver(reloadUserListener)
        } catch (e: Exception) {
        }
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            101 -> {
                if (resultCode == 1) {
                    contactFragment.loadContacts()
                }
            }

        }
    }

    private fun checkMeetingState() {
        val meeting = getMeetingState()

        if (meeting != null) {
            ApiClient.getApi().getMeeting(meeting.id).enqueue(object : Callback<ApiData.Meeting> {
                override fun onFailure(call: Call<ApiData.Meeting>?, t: Throwable?) {
                    Toast.makeText(this@MainActivity, "Unable to check meeting state", Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<ApiData.Meeting>?, response: Response<ApiData.Meeting>?) {
                    if (response?.code() == 200) {
                        if (response.body()?.status != "active") {
                            setInMeeting(null)
                        } else {
                            setInMeeting(response.body()!!)
                            setAppInMeeting(response.body()!!)
                        }
                    }
                }
            })
        } else {
            ApiClient.getApi().getLastState(getUserData()?.id
                    ?: -1).enqueue(object : Callback<ApiData.Meeting> {
                override fun onFailure(call: Call<ApiData.Meeting>?, t: Throwable?) {
                    Toast.makeText(this@MainActivity, "Unable to latest meeting state", Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<ApiData.Meeting>?, response: Response<ApiData.Meeting>?) {
                    if (response?.code() == 200) {
                        if (response.body()?.status != "active") {
                            setInMeeting(null)
                        } else {
                            setInMeeting(response.body()!!)
                            setAppInMeeting(response.body()!!)
                        }
                    }
                }
            })
        }
    }

    private fun setAppInMeeting(meeting: ApiData.Meeting) {
        val i = Intent(this, ActivityInMeeting::class.java)
        i.putExtra("meeting", ApiClient.getGson().toJson(meeting))
        startActivity(i)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            10 -> {
                if (grantResults.size > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    checkForPermission()
                } else {
                    Toast.makeText(this, "Please allow application to use Location Permission", Toast.LENGTH_LONG).show()
                    checkForPermission()
                }
            }
            11 -> {
                if (grantResults.size > 0 && grantResults[0] == PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this, "Please allow application to use Location Permission", Toast.LENGTH_LONG).show()
                    checkForPermission()
                }
            }
        }
    }

    private fun checkForPermission() {
        if (ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(ACCESS_COARSE_LOCATION), 10)
            return
        }
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), 11)
            return
        }

        val i = Intent(this, LocationSender::class.java)
        startService(i)
    }

    class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        val data = mutableListOf<Page>()
        override fun getItem(position: Int): Fragment = data[position].page

        override fun getCount(): Int = data.size

        override fun getPageTitle(position: Int): CharSequence? = data[position].title
    }

}
