package com.exairie.meet


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.exairie.meet.api.ApiClient
import com.exairie.meet.api.ApiData
import kotlinx.android.synthetic.main.fragment_contactitem_container.*
import kotlinx.android.synthetic.main.view_meet_history.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MeetingFragment : Fragment() {
    val adapter = Adapter()
    lateinit var pref : SharedPreferences
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contactitem_container,null)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler_contactitem.adapter = adapter
        recycler_contactitem.layoutManager = LinearLayoutManager(context)
        pref = activity!!.getSharedPreferences(context!!.packageName, Context.MODE_PRIVATE)

        refresher.setOnRefreshListener {
            loadData()
        }

        loadData()
    }

    fun loadData(){
        launch(UI){
            refresher.isRefreshing = true
        }

    }

    fun getUserData() : ApiData.LoginData?{
        val udata = pref.getString("userdata","null")
        if(udata == "null") return null
        return ApiClient.getGson().fromJson(udata, ApiData.LoginData::class.java)
    }

    inner class Adapter : RecyclerView.Adapter<Holder>(){
        val data = mutableListOf<ApiData.Meeting>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_meet_history,parent, false)
            v.img_profile.setImageResource(R.drawable.ic_arrow_shrink)
            return Holder(v)
        }

        override fun getItemCount(): Int = data.size
        override fun onBindViewHolder(holder: Holder, position: Int) {
            val d = data[position]
            with(holder.itemView) {
                l_initiator.text = "Initiator: ${d.user?.firstname}"
                l_meetdate.text = "${android.text.format.DateFormat.format("dd MMM yyyy HH:mm",d.created_at)}"
                l_status.text = "${d.status}"
                l_participants.text = "${d.meeting_members?.size ?: "n/a"} Participants"
            }
        }
    }

}
