package com.exairie.meet.api

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.*

interface ApiAccessInterface {
    @POST("register")
    fun registerUserAccount(@Body data: ApiData.LoginData): Call<ApiData.LoginData?>

    @GET("user/{id}/meetings")
    fun getUserMeetings(@Path("id") userid: Long): Call<Array<ApiData.Meeting>>

    @GET("user/{id}/contacts")
    fun getContacts(@Path("id") userid: Long): Call<Array<ApiData.Contact>>

    @POST("user/{id}/set/guide")
    fun setUserAsGuide(@Path("id") userid: Long): Call<ApiData.LoginData>

    @GET("users")
    fun searchUsers(@QueryMap search: HashMap<String, String>): Call<Array<ApiData.LoginData>>

    @POST("user/{id}/contacts/add/{contactid}")
    fun addContact(@Path("id") userid: Long, @Path("contactid") contactid: Long): Call<Any>

    @POST("updatelocation")
    fun updateLocation(@Body pos: ApiData.Position): Call<Any>

    @POST("meeting/create")
    fun createMeeting(@Body req: ApiData.MeetingRequest): Call<ApiData.Meeting>

    @GET("state/{id}")
    fun getLastState(@Path("id") id: Long): Call<ApiData.Meeting>

    @GET("meeting/{id}")
    fun getMeeting(@Path("id") id: Long): Call<ApiData.Meeting>

    @GET("meeting/{mid}/track/{uid}")
    fun track(@Path("mid") meetingid: Long, @Path("uid") userid: Long): Call<Array<ApiData.Position>>

    @POST("meeting/positions")
    fun getPositions(@Body data : ApiData.PositionQuery): Call<Array<ApiData.Position>>

    @GET("meeting/{id}/finish")
    fun finishMeeting(@Path("id") id: Long): Call<Any>

    @GET("groups/{id}")
    fun getGuideGroups(@Path("id") id: Long): Call<Array<ApiData.Group>>

    @POST("destination")
    fun createTourDestination(@Body data: ApiData.TourDestination): Call<ApiData.TourDestination>

    @GET("destination/{id}/get")
    fun getGuideDestination(@Path("id") id: Long): Call<Array<ApiData.TourDestination>>

    @POST("groups/{initiator}/create")
    fun createGroupWithMember(@Path("initiator") initiatorId: Long, @Body data: ApiData.GroupInput): Call<ApiData.Group>

    @GET("groupof/{userid}")
    fun getUserTourGroups(@Path("userid") userid: Long): Call<Array<ApiData.Group>>

    @GET("groups/{groupid}/chats")
    fun getGroupChats(@Path("groupid") groupid: Long): Call<Array<ApiData.GroupChat>>

    @POST("group/chat")
    fun sendGroupChat(@Body chatInfo: ApiData.GroupChat): Call<ApiData.GroupChat>

    @GET("groups/{group}/info")
    fun getGroupInfo(@Path("group") id : Long): Call<ApiData.Group>

    @POST("groups/{group}/destination/{destination}")
    fun setGroupDestination(@Path("group") id : Long,@Path("group") destId : Long): Call<ApiData.Group>

    @DELETE("groups/{group}/destination")
    fun clearGroupDestination(@Path("group") id : Long): Call<ApiData.Group>
}
