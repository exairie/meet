package com.exairie.meet.api

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient{
    companion object {
        val baseUrl = "http://datditdut.com/meet/public/api/"
        var retrofit : Retrofit? = null
        var googleRetrofit : Retrofit? = null
        fun getGson() : Gson {
            return GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()
        }
        fun saveLoggedUserData(context: Context, data: ApiData.LoginData?){
            val pref = context.getSharedPreferences(context.packageName,Context.MODE_PRIVATE)

            if(data != null){
                pref.edit()
                        .putString("userdata",ApiClient.getGson().toJson(data))
                        .apply()
            }else{
                pref.edit()
                        .remove("userdata")
                        .apply()
            }
        }
        fun getLoggedInUserData(context : Context):ApiData.LoginData?{

            val pref = context.getSharedPreferences(context.packageName,Context.MODE_PRIVATE)

            val udata = pref.getString("userdata","null")
            return ApiClient.getGson().fromJson(udata, ApiData.LoginData::class.java)
        }
        fun getClient() : Retrofit{
            if(retrofit == null){
                val gson = getGson()
                retrofit = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
            }

            return retrofit!!
        }

        fun getApi() : ApiAccessInterface{
            val retrofit = getClient()
            return retrofit!!.create(ApiAccessInterface::class.java)
        }

        fun getGoogleClient() : Retrofit{
            if(googleRetrofit == null){
                val gson = getGson()
                googleRetrofit = Retrofit.Builder()
                        .baseUrl("http://maps.google.com/maps/api/")
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
            }

            return googleRetrofit!!
        }
    }



}