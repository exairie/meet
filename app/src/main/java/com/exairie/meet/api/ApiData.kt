package com.exairie.meet.api

import com.google.gson.annotations.SerializedName
import java.util.*

class ApiData {
    data class LoginData(
            val google_auth_code: String?,
            val firstname: String?,
            val lastname: String?,
            val email: String?,
            val phone: String?,
            val token: String?,
            val id: Long = 0,
            val photo_url: String?,
            val is_guide: Int,
            val created_at: Date? = null,
            val updated_at: Date? = null,
            val user_positions: Array<Position>? = arrayOf()

    )

    data class MeetingMember(
            val id: Long,
            val user_id: Long,
            val meeting_id: Long,
            val user: LoginData? = null,
            val created_at: Date? = null,
            val updated_at: Date? = null
    )

    data class Meeting(
            val id: Long,
            val initiator: Long,
            val meet_eta: Date,
            val status: String?,
            val created_at: Date,
            val updated_at: Date,
            val lat: Double,
            val lng: Double,
            val meeting_members: Array<MeetingMember>?,
            val user: LoginData?
    )

    data class PositionQuery(
        val ids : Array<Long>
    )

    open class Group(
            @SerializedName("active")
            val active: Int,
            @SerializedName("active_waypoint")
            val activeWaypoint: Any,
            @SerializedName("created_at")
            val createdAt: Date,
            @SerializedName("updated_at")
            val updatedAt: Date,
            @SerializedName("description")
            val description: String,
            @SerializedName("group_name")
            val groupName: String,
            @SerializedName("id")
            val id: Long,
            @SerializedName("initiator")
            val initiator: Long,

            @SerializedName("initiated_by")
            val initiated_by: LoginData?,

            @SerializedName("tour_group_members")
            val tour_group_members: Array<TourGroupMember>?,

            @SerializedName("tourDestination")
            val tourDestination : ApiData.TourDestination?
    )

    data class TourGroupMember(
            @SerializedName("admin")
            val admin: Int,
            @SerializedName("created_at")
            val createdAt: String,
            @SerializedName("id")
            val id: Long,
            @SerializedName("tour_group_id")
            val tourGroupId: Long,
            @SerializedName("updated_at")
            val updatedAt: String,
            @SerializedName("user_id")
            val userId: Long,

            @SerializedName("user")
            val user : LoginData?
    )

    data class ChatEmitData(val roomId: Long, val chatInfo: GroupChat)


    data class GroupInput(
            var active: Int,
            var description: String,
            @SerializedName("group_name")
            var group_name: String,
            var initiator: Long,
            var members: MutableList<Long>
    )

    data class TourDestination(
            @SerializedName("created_at")
            val createdAt: Date,
            @SerializedName("creator")
            val creator: Long,
            @SerializedName("destination_name")
            val destinationName: String,
            @SerializedName("description")
            val description: String,
            @SerializedName("geofences")
            val geofences: String,
            @SerializedName("id")
            val id: Int,
            @SerializedName("latitude")
            val latitude: Double,
            @SerializedName("longitude")
            val longitude: Double,
            @SerializedName("radius")
            val radius: Double,
            @SerializedName("updated_at")
            val updatedAt: Date,

            @SerializedName("userCreator")
            val userCreator: LoginData?
    )


    data class Position(
            val id: Long,
            val user_id: Long,
            val lat: Double,
            val lng: Double,
            val speed: Float,
            val heading: Float,
            val location_name: String?,
            val created_at: Date,
            val updated_at: Date
    )

    data class Contact(
            val id: Long,
            val user_id: Long,
            val contact: LoginData?
    )

    data class GroupChat(
            @SerializedName("chat_text")
            val chatText: String,
            @SerializedName("created_at")
            val createdAt: Date?,
            @SerializedName("group_id")
            val groupId: Long,
            @SerializedName("id")
            val id: Long?,
            @SerializedName("updated_at")
            val updatedAt: Date?,
            @SerializedName("user_id")
            val userId: Long,

            @SerializedName("user")
            val user: LoginData?
    )

    data class MeetingRequest(
            val meeting: Meeting,
            val members: Array<String>
    )


    data class GoogleMapGeocodeResult(
            val results: Array<Result>,
            val status: String
    )

    data class Result(
            val address_components: Array<AddressComponent>,
            val formatted_address: String,
            val geometry: Geometry,
            val place_id: String,
            val types: Array<String>
    )

    data class Geometry(
            val bounds: Bounds,
            val location: Location,
            val location_type: String,
            val viewport: Viewport
    )

    data class Bounds(
            val northeast: Northeast,
            val southwest: Southwest
    )

    data class Southwest(
            val lat: Double,
            val lng: Double
    )

    data class Northeast(
            val lat: Double,
            val lng: Double
    )

    data class Location(
            val lat: Double,
            val lng: Double
    )

    data class Viewport(
            val northeast: Northeast,
            val southwest: Southwest
    )

    data class AddressComponent(
            val long_name: String,
            val short_name: String,
            val types: Array<String>
    )

}