package com.exairie.meet.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface GoogleMapsApi {
    @GET("geocode/json")
    fun geocodeAddress(@QueryMap query : HashMap<String,String>) : Call<ApiData.GoogleMapGeocodeResult>
}